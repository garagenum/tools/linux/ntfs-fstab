#!/bin/bash

#A script to register a ntfs partition in /etc/fstab
#Repository at https://gitlab.com/sakura-lain/ntfs-fstab

main(){
	
	echo -e "\033[34;1m\n#########################################################
## A script to register a NTFS partition in /etc/fstab ##
#########################################################
	\033[31;1m\nThis script must be run as root and the desired partition must be previoulsy mounted.\e[0m"

	roottesting
	
	echo -e "\033[1m\nThe following NTFS partitions are available for mounting:\e[0m\n"
	lsblk -f | grep ntfs #Lists every available NTFS partition
	echo -e "\033[1m\nWhich partition do you want to mount at startup (specify using the form /dev/sdaX)?\e[0m"
	read PARTITION

	#Variables :
	MOUNT=$(lsblk -no MOUNTPOINT $PARTITION) #Identifies the present mountpoint
	UUID=$(lsblk -no UUID $PARTITION) #Identifies the partition's UUID number
	CHECKUUID=$(grep $UUID < /etc/fstab | cut -d ' ' -f1 | cut -d '=' -f2) #Identifies the partition's UUID in /etc/fstab 
	CHECKPART=$(lsblk -no NAME $PARTITION) #Identifies the partition in the list of existing partitions
	CHECKFS=$(lsblk -no FSTYPE $PARTITION) #Identifies the partition's filesystem
	
	if [[ /dev/$CHECKPART == $PARTITION ]] #Checks that the partition exists
	then
		if [[ -z $MOUNT ]] #Checks whether the partition appears in the list of mounted partitions
		then
			echo -e "\033[1;31m\nPlease mount the partition you want to register!\e[0m\n"
		else

			if [[ $CHECKUUID != $UUID ]] #Checks that the partition is not already present in /etc/fstab, using its UUID
			then
    
      	        		if [[ $CHECKFS == ntfs ]] #Checks the partition's filesystem
        			then
                    			mountpoint	
	                		fstab
				else
					echo -e "\033[1;31m\nBad filesystem!\e[0m\n"
					retry
                		fi
		
			else
				echo -e "\033[1;31m\nThe partition is already registered in /etc/fstab!\e[0m\n"
				retry
			fi

		fi

	else
		echo -e "\033[1;31m\nThe partition can't be found!\e[0m\n"
		retry
	fi

}

roottesting(){

	if [[ $(whoami) != root ]]
	then
		echo -e "\033[31;1m\nPlease run as root!\e[0m"
		exit
	fi

}

mountpoint(){

	echo -e "\033[0;1m\nPlease specify a mountpoint (it will be created if it doesn't exist yet):\e[0m"
	read NEWMOUNT

	#Variable:
	CHECKMOUNT=$(cat /etc/fstab | grep -w "^[^#]*$NEWMOUNT" | awk '{ print $2 }') #Identifies the desired mountpoint in /etc/fstab

	if [[ $CHECKMOUNT != $NEWMOUNT ]] #Checks that the desired mountpoint doesn't already appear in /etc/fstab
	then

		if [[ ! -d $NEWMOUNT ]]
		then
			mkdir $NEWMOUNT
		
			if [[ $? != 0 ]]
			then
				echo -e "\033[31;1m\nCannot create the mountpoint!\e[0m"
				retry
			fi
		
		fi
	else
        echo -e "\033[1;31m\nThe mountpoint you wish to set is already registered in /etc/fstab!\e[0m\n"
		retry
	fi

}

fstab(){

	#Variable :
	MTAB=$(grep $PARTITION < /etc/mtab | awk '{print $2, $3, $4, $5, $6}')
	
	cp /etc/fstab /etc/fstab.bak #/etc/fstab backup
	echo -e "#NTFS partition on $PARTITION
UUID=$UUID $MTAB" | sed -e "s/fuseblk/ntfs-3g/g" -e "s+$MOUNT+$NEWMOUNT+g"  >> /etc/fstab #Replaces former mountpoint with new mountpoint and filesystem fuseblk with ntfs-3g in $MTAB, then concatenates the data and writes them at the end of /etc/fstab
	echo -e "\033[1;31m\nSuccess!\e[0m"

	echo -e "\033[1m\nView new fstab? (y/n)\e[0m"
	read ANSWER
	echo -e "\n"
	
	if [[ $ANSWER == y || $ANSWER == Y || $ANSWER == yes || $ANSWER == Yes || $ANSWER == YES ]]
	then
		cat /etc/fstab
		retry
	else
		retry
	fi

}

retry(){
    
    	echo -e "\033[0;1m\nPlay again? (y/n)\e[0m"
    	read ANSWER
	echo -e "\n"
	
	if [[ $ANSWER == y || $ANSWER == Y || $ANSWER == yes || $ANSWER == Yes || $ANSWER == YES ]]
	then
		$0 #Runs the script again
	else
		exit
	fi
    
}

main 2> /dev/null #Calls main function without displaying error messages
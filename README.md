# Register a NTFS partition in /etc/fstab

## A bash script to register a NTFS partition in `/etc/fstab` in order to allow automount at startup.

Must be run as root, with the desired partition previously mounted. Don't forget to `chmod +x`.

The script interacts with the user to chose which partition to register and which mountpoint to chose, then, after a series of checkings, copies data gathered via `lsblk` and in `/etc/mtab` in `/etc/fstab`. At the end the user can check the result while reading the new `/etc/fstab`.